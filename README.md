# syncpasswd

Simple GNOME application to sync your password.

![SyncPasswd logo](https://gitlab.gnome.org/gnumdk/syncpasswd/-/raw/screenshots/syncpasswd.png?ref_type=heads&inline=true)

On laptops connected to Active Directory/LDAP with SSSD, remote users may be locked with previous password (PAM auth then VPN connection). So cached SSSD password is always the previous one.

This app allows to:
- sync SSSD cached password (or enable another account in cache)
- update keyring master password
- update user password if found in default keyring
- update user password if found in Firefox Sync
- update 802.1x password in Network Manager

For sysadmins, you can enable SSSD password management with dconf. LDAP server is needed to check if service is available (asking user to use a VPN otherwise)

https://help.gnome.org/admin/system-admin-guide/stable/dconf-keyfiles.html.en

```
[org/adishatz/syncpasswd]
domain="MyDomain"

[org/adishatz/syncpasswd]
ldap-server="server.domain"

# By default
[org/adishatz/syncpasswd]
ldap-port=636
```

You also need to enabled this:
`flatpak [--user] override org.adishatz.syncpasswd --talk-name=org.freedesktop.Flatpak`
