# main.py
#
# Copyright 2023 Cédric Bellegarde
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

import sys
import gi

gi.require_version('Gtk', '4.0')
gi.require_version('Adw', '1')
gi.require_version('Secret', '1')
gi.require_version("NM", "1.0")

from gi.repository import Gtk, Gdk, Gio, Adw
from .firefox_dialog import FirefoxDialog
from .window import SyncPasswdWindow


class SyncPasswdApplication(Adw.Application):

    def __init__(self, version):
        """
            Create application
        """
        super().__init__(application_id='org.adishatz.syncpasswd',
                         flags=Gio.ApplicationFlags.FLAGS_NONE)
        self.version = version
        self.create_action('quit', self.quit, ['<primary>q'])
        self.create_action('firefox', self.on_firefox_action)
        self.create_action('about', self.on_about_action)
        css = Gio.File.new_for_uri(
            "resource:////org/adishatz/syncpasswd/style.css"
        )
        provider = Gtk.CssProvider()
        provider.load_from_file(css)
        display = Gdk.Display.get_default()
        Gtk.StyleContext.add_provider_for_display(
            display, provider, Gtk.STYLE_PROVIDER_PRIORITY_USER
        )


    def do_activate(self):
        """
            Add a window if non present
        """
        win = self.props.active_window
        if not win:
            win = SyncPasswdWindow(application=self)
        win.present()

    def on_firefox_action(self, widget, _):
        """
            Show Firefox connect dialog
        """
        dialog = FirefoxDialog(self.props.active_window);
        dialog.present()

    def on_about_action(self, widget, _):
        """
            Show about dialog
        """
        about = Adw.AboutWindow(transient_for=self.props.active_window,
                                application_name='Sync Password',
                                application_icon='org.adishatz.syncpasswd',
                                developer_name='Cédric Bellegarde',
                                version=self.version,
                                developers=['Cédric Bellegarde'],
                                copyright='© 2023 Cédric Bellegarde')
        about.present()

    def on_preferences_action(self, widget, _):
        pass

    def create_action(self, name, callback, shortcuts=None):
        """
            Add an application action.

            Args:
                name: the name of the action
                callback: the function to be called when the action is
                  activated
                shortcuts: an optional list of accelerators
        """
        action = Gio.SimpleAction.new(name, None)
        action.connect("activate", callback)
        self.add_action(action)
        if shortcuts:
            self.set_accels_for_action(f"app.{name}", shortcuts)


def main(version):
    """
        The application's entry point
    """
    app = SyncPasswdApplication(version)
    return app.run(sys.argv)
