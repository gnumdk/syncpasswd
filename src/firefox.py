# firefox.py
#
# Copyright 2017-2023 Cédric Bellegarde
# Fork of https://github.com/firefox-services/syncclient
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

import json
import six

from gi.repository import GObject, GLib

from time import time
from hashlib import sha256
from base64 import b64encode, b64decode
from fxa.core import Client as FxAClient
from fxa.core import Session as FxASession
from fxa.crypto import quick_stretch_password
from binascii import hexlify
from requests import request, exceptions
from requests_hawk import HawkAuth
from hmac import new
from math import ceil
from threading import Thread

TOKENSERVER_URL = "https://token.services.mozilla.com/"

class Firefox(GObject.Object):
    __gsignals__ = {
        "verify-account": (GObject.SignalFlags.RUN_FIRST, None, ()),
        "connected": (GObject.SignalFlags.RUN_FIRST, None, (bool,)),
        "password-finished": (GObject.SignalFlags.RUN_FIRST, None, (str,))
    }

    class Status:
        NOT_CONNECTED = 0
        CONNECTED = 1
        VERIFY = 2
        ERROR = 3

    def __init__(self, secrets):
        GObject.Object.__init__(self)
        self.status = Firefox.Status.NOT_CONNECTED
        self.secrets = secrets
        self.session = None
        self.firefox_sync = None
        secrets.get_firefox_sync(self.on_get_firefox_sync)

    @property
    def connected(self):
        return self.session is not None and\
            self.status == Firefox.Status.CONNECTED

    def logon(self, username, password, twofa):
        """
            Logon Firefox Sync
        """
        if not username or not password:
            print("Invalid credentials!")
            return

        thread = Thread(
            target=self.logon_async,
            args=(username, password, twofa)
        )
        thread.daemon = True
        thread.start()

    def logout(self):
        """
            Logout Firefox Sync
        """
        self.session = None
        self.secrets.clear_firefox_sync()

    def reset_secrets(self, old_password, new_password):
        """
            Reset secrets with a new password
        """
        thread = Thread(
            target=self.reset_secrets_async,
            args=(old_password, new_password)
        )
        thread.daemon = True
        thread.start()

    def init_session(self, username, uid, token, keyB):
        """
            Init FxASession
        """
        thread = Thread(
            target=self.init_session_async,
            args=(username, uid, token, keyB)
        )
        thread.daemon = True
        thread.start()

    def init_session_async(self, username, uid, token, keyB):
        try:
            if self.firefox_sync is None:
                self.firefox_sync = FirefoxSync()

            self.session = FxASession(
                self.firefox_sync.fxa_client,
                username,
                quick_stretch_password(
                    username,
                    ""
                ),
                uid,
                token
            )
            self.session.keys = [b"", keyB]
            self.get_session_bulk_keys()
            GLib.idle_add(self.emit, "connected", True)
            self.status = Firefox.Status.CONNECTED
        except Exception as e:
            print(e)
            GLib.idle_add(self.emit, "connected", False)
            self.status = Firefox.Status.ERROR

    def logon_async(self, username, password, twofa):
        if self.firefox_sync is None:
            self.firefox_sync = FirefoxSync()

        try:
            self.session = self.firefox_sync.connect(
                username, password, twofa)
            bid_assertion, key = self.firefox_sync.get_browserid_assertion(
                self.session
            )
            keyB_encoded = b64encode(self.session.keys[1]).decode("utf-8")
            record = {
                "uid": self.session.uid,
                "token": self.session.token,
                "keyB": keyB_encoded
            }
            self.secrets.clear_firefox_sync(
                self.secrets.set_firefox_sync,
                username,
                json.dumps(record)
            )
            GLib.idle_add(self.emit, "connected", True)
            self.status = Firefox.Status.CONNECTED
        except Exception as e:
            print(e)
            if str(e) == "Unconfirmed account":
                GLib.idle_add(self.emit, "verify-account")
                self.status = Firefox.Status.VERIFY
            else:
                GLib.idle_add(self.emit, "connected", False)
                self.status = Firefox.Status.ERROR

    def reset_secrets_async(self, old_password, new_password):
        message = None
        count = 0
        try:
            secrets = []
            bulk_keys = self.get_session_bulk_keys()
            records = self.firefox_sync.get_records("passwords", bulk_keys)
            for record in records:
                secret = record["payload"]
                if "password" in secret.keys() and\
                        secret["password"] == old_password:
                    secrets.append(secret)

            for secret in secrets:
                secret["password"] = new_password
                secret["timePasswordChanged"] = time()
                self.firefox_sync.add(secret, "passwords", bulk_keys)
                count += 1
        except Exception as e:
            print(e)
        if count == 1:
            message = _("One password updated in Firefox")
        elif count > 1:
            message = _("%s passwords updated in Firefox" % count)
        GLib.idle_add(self.emit, "password-finished", message)

    def get_session_bulk_keys(self):
        """
            Get session decrypt keys
        """
        if self.session is None:
            return None

        self.session.check_session_status()
        bid_assertion, key = self.firefox_sync.get_browserid_assertion(
            self.session
        )
        return self.firefox_sync.get_bulk_keys(bid_assertion, key)

    def on_get_firefox_sync(self, attributes, password, index, count):
        try:
            if attributes is None:
                return
            record = json.loads(password)
            self.init_session(
                attributes["login"],
                record["uid"],
                record["token"],
                b64decode(record["keyB"])
            )
        except Exception as e:
            print(e)


class FirefoxSync(object):
    """
        Sync client
    """

    def __init__(self):
        """
            Init client
        """
        self.fxa_client = FxAClient()
        self.sync_client = None

    def connect(self, username, password, twofa):
        """
            Connect to Firefox Sync
        """
        session = self.fxa_client.login(username, password, keys=True)
        if twofa:
            session.totp_verify(twofa)
        session.fetch_keys()
        return session

    def get_bulk_keys(self, bid_assertion, key):
        """
            Get Firefox Sync bulk keys
        """
        state = None
        if key is not None:
            state = hexlify(sha256(key).digest()[0:16])
        self.sync_client = SyncClient(bid_assertion, state)
        sync_keys = KeyBundle.fromMasterKey(
            key,
            "identity.mozilla.com/picl/v1/oldsync")

        # Fetch the sync bundle keys out of storage.
        # They're encrypted with the account-level key.
        keys = self.decrypt_payload(
            self.sync_client.get_record(
                "crypto", "keys"),
                sync_keys
        )

        # There's some provision for using separate
        # key bundles for separate collections
        # but I haven't bothered digging through
        # to see what that's about because
        # it doesn't seem to be in use, at least on my account.
        if keys["collections"]:
            print("""no support for per-collection
                     key bundles yet sorry :-(""")
            return None

        # Now use those keys to decrypt the records of interest.
        bulk_keys = KeyBundle(b64decode(keys["default"][0]),
                              b64decode(keys["default"][1]))
        return bulk_keys

    def add(self, item, collection, bulk_keys):
        """
            Add item to collection
        """
        payload = self.encrypt_payload(item, bulk_keys)
        record = {}
        record["modified"] = round(time(), 2)
        record["payload"] = payload
        record["id"] = item["id"]
        self.sync_client.put_record(collection, record)

    def get_records(self, collection, bulk_keys):
        """
            Get payload from collection
        """
        records = []
        for record in self.sync_client.get_records(collection):
            record["payload"] = self.decrypt_payload(record, bulk_keys)
            records.append(record)
        return records

    def get_browserid_assertion(self, session,
                                tokenserver_url=TOKENSERVER_URL):
        """
            Get browser id assertion and state
            @param session as fxaSession
            @return (bid_assertion, state) as (str, str)
        """
        bid_assertion = session.get_identity_assertion(tokenserver_url)
        return bid_assertion, session.keys[1]

    def encrypt_payload(self, payload, key_bundle):
        from Crypto.Cipher import AES
        from Crypto import Random
        from hmac import new
        from base64 import b64encode
        plaintext = json.dumps(payload).encode("utf-8")
        # Input strings must be a multiple of 16 in length
        length = 16 - (len(plaintext) % 16)
        plaintext += bytes([length]) * length
        iv = Random.new().read(16)
        aes = AES.new(key_bundle.encryption_key, AES.MODE_CBC, iv)
        ciphertext = b64encode(aes.encrypt(plaintext))
        _hmac = new(key_bundle.hmac_key, ciphertext, sha256).hexdigest()
        payload = {"ciphertext": ciphertext.decode("utf-8"),
                   "IV": b64encode(iv).decode("utf-8"), "hmac": _hmac}
        return json.dumps(payload)

    def decrypt_payload(self, record, key_bundle):
        from Crypto.Cipher import AES
        from hmac import new
        from base64 import b64decode
        j = json.loads(record["payload"])
        # Always check the hmac before decrypting anything.
        expected_hmac = new(key_bundle.hmac_key,
                            j['ciphertext'].encode("utf-8"),
                            sha256).hexdigest()
        if j['hmac'] != expected_hmac:
            raise ValueError("HMAC mismatch: %s != %s" % (j['hmac'],
                                                          expected_hmac))
        ciphertext = b64decode(j['ciphertext'])
        iv = b64decode(j['IV'])
        aes = AES.new(key_bundle.encryption_key, AES.MODE_CBC, iv)
        plaintext = aes.decrypt(ciphertext).strip().decode("utf-8")
        # Remove any CBC block padding,
        # assuming it's a well-formed JSON payload.
        plaintext = plaintext[:plaintext.rfind("}") + 1]
        return json.loads(plaintext)


class KeyBundle:
    """
        RFC-5869
    """

    def __init__(self, encryption_key, hmac_key):
        self.encryption_key = encryption_key
        self.hmac_key = hmac_key

    @classmethod
    def fromMasterKey(cls, master_key, info):
        key_material = KeyBundle.HKDF(master_key, None, info, 2 * 32)
        return cls(key_material[:32], key_material[32:])

    def HKDF_extract(salt, IKM, hashmod=sha256):
        """
            Extract a pseudorandom key suitable for use with HKDF_expand
        """
        from hmac import new
        if salt is None:
            salt = b"\x00" * hashmod().digest_size
        return new(salt, IKM, hashmod).digest()

    def HKDF_expand(PRK, info, length, hashmod=sha256):
        """
            Expand pseudo random key and info
        """
        digest_size = hashmod().digest_size
        N = int(ceil(length * 1.0 / digest_size))
        assert N <= 255
        T = b""
        output = []
        for i in range(1, N + 1):
            data = T + (info + chr(i)).encode()
            T = new(PRK, data, hashmod).digest()
            output.append(T)
        return b"".join(output)[:length]

    def HKDF(secret, salt, info, length, hashmod=sha256):
        """
            HKDF-extract-and-expand as a single function.
        """
        PRK = KeyBundle.HKDF_extract(salt, secret, hashmod)
        return KeyBundle.HKDF_expand(PRK, info, length, hashmod)


class TokenserverClient(object):
    """
        Client for the Firefox Sync Token Server.
    """

    def __init__(self, bid_assertion, client_state,
                 server_url=TOKENSERVER_URL):
        self.bid_assertion = bid_assertion
        self.client_state = client_state
        self.server_url = server_url

    def get_hawk_credentials(self, duration=None):
        """
            Asks for new temporary token given a BrowserID assertion
            @param duration as str
        """
        from requests import get
        authorization = 'BrowserID %s' % self.bid_assertion
        headers = {
            'Authorization': authorization,
            'X-Client-State': self.client_state
        }
        params = {}

        if duration is not None:
            params['duration'] = int(duration)

        url = self.server_url.rstrip('/') + '/1.0/sync/1.5'
        raw_resp = get(url, headers=headers, params=params, verify=True)
        raw_resp.raise_for_status()
        return raw_resp.json()


class SyncClient(object):
    """
        Client for the Firefox Sync server.
    """

    def __init__(self, bid_assertion=None, client_state=None,
                 credentials={}, tokenserver_url=TOKENSERVER_URL):
        if bid_assertion is not None and client_state is not None:
            ts_client = TokenserverClient(bid_assertion, client_state,
                                          tokenserver_url)
            credentials = ts_client.get_hawk_credentials()
        self.user_id = credentials['uid']
        self.api_endpoint = credentials['api_endpoint']
        self.auth = HawkAuth(
            algorithm=credentials['hashalg'],
            id=credentials['id'],
            key=credentials['key'],
            always_hash_content=False
        )

    def _request(self, method, url, **kwargs):
        """
            Utility to request an endpoint with the correct authentication
            setup, raises on errors and returns the JSON.
        """
        url = self.api_endpoint.rstrip('/') + '/' + url.lstrip('/')
        raw_resp = request(method, url, auth=self.auth, **kwargs)
        raw_resp.raise_for_status()

        if raw_resp.status_code == 304:
            http_error_msg = '%s Client Error: %s for url: %s' % (
                raw_resp.status_code,
                raw_resp.reason,
                raw_resp.url)
            raise exceptions.HTTPError(http_error_msg, response=raw_resp)
        return raw_resp.json()

    def info_collections(self, **kwargs):
        """
            Returns an object mapping collection names associated with the
            account to the last-modified time for each collection.

            The server may allow requests to this endpoint to be authenticated
            with an expired token, so that clients can check for server-side
            changes before fetching an updated token from the Token Server.
        """
        return self._request('get', '/info/collections', **kwargs)

    def info_quota(self, **kwargs):
        """
            Returns a two-item list giving the user's current usage and quota
            (in KB). The second item will be null if the server
            does not enforce quotas.

            Note that usage numbers may be approximate.
        """
        return self._request('get', '/info/quota', **kwargs)

    def get_collection_usage(self, **kwargs):
        """
            Returns an object mapping collection names associated with the
            account to the data volume used for each collection (in KB).

            Note that these results may be very expensive as it calculates more
            detailed and accurate usage information than the info_quota method.
        """
        return self._request('get', '/info/collection_usage', **kwargs)

    def get_collection_counts(self, **kwargs):
        """
            Returns an object mapping collection names associated with the
            account to the total number of items in each collection.
        """
        return self._request('get', '/info/collection_counts', **kwargs)

    def delete_all_records(self, **kwargs):
        """
            Deletes all records for the user
        """
        return self._request('delete', '/', **kwargs)

    def get_records(self, collection, full=True, ids=None, newer=None,
                    limit=None, offset=None, sort=None, **kwargs):
        """
            Returns a list of the BSOs contained in a collection. For example:

            >>> ["GXS58IDC_12", "GXS58IDC_13", "GXS58IDC_15"]

            By default only the BSO ids are returned, but full objects can be
            requested using the full parameter. If the collection does not
            exist, an empty list is returned.

            :param ids:
                a comma-separated list of ids. Only objects whose id is in
                this list will be returned. A maximum of 100 ids may be
                provided.

            :param newer:
                a timestamp. Only objects whose last-modified time is strictly
                greater than this value will be returned.

            :param full:
                any value. If provided then the response will be a list of full
                BSO objects rather than a list of ids.

            :param limit:
                a positive integer. At most that many objects will be returned.
                If more than that many objects matched the query,
                an X-Weave-Next-Offset header will be returned.

            :param offset:
                a string, as returned in the X-Weave-Next-Offset header of a
                previous request using the limit parameter.

            :param sort:
                sorts the output:
                "newest" - orders by last-modified time, largest first
                "index" - orders by the sortindex, highest weight first
                "oldest" - orders by last-modified time, oldest first
        """
        params = kwargs.pop('params', {})
        if full:
            params['full'] = True
        if ids is not None:
            params['ids'] = ','.join(map(str, ids))
        if newer is not None:
            params['newer'] = newer
        if limit is not None:
            params['limit'] = limit
        if offset is not None:
            params['offset'] = offset
        if sort is not None and sort in ('newest', 'index', 'oldest'):
            params['sort'] = sort

        return self._request('get', '/storage/%s' % collection.lower(),
                             params=params, **kwargs)

    def get_record(self, collection, record_id, **kwargs):
        """Returns the BSO in the collection corresponding to the requested id.
        """
        return self._request('get', '/storage/%s/%s' % (collection.lower(),
                                                        record_id), **kwargs)

    def delete_record(self, collection, record_id, **kwargs):
        """Deletes the BSO at the given location.
        """
        try:
            return self._request('delete', '/storage/%s/%s' % (
                collection.lower(), record_id), **kwargs)
        except Exception as e:
            Logger.error("SyncClient::delete_record(): %s", e)

    def put_record(self, collection, record, **kwargs):
        """
            Creates or updates a specific BSO within a collection.
            The passed record must be a python object containing new data for
            the BSO.

            If the target BSO already exists then it will be updated with the
            data from the request body. Fields that are not provided will not
            be overwritten, so it is possible to e.g. update the ttl field of a
            BSO without re-submitting its payload. Fields that are explicitly
            set to null in the request body will be set to their default value
            by the server.

            If the target BSO does not exist, then fields that are not provided
            in the python object will be set to their default value
            by the server.

            Successful responses will return the new last-modified time for the
            collection.

            Note that the server may impose a limit on the amount of data
            submitted for storage in a single BSO.
        """
        # XXX: Workaround until request-hawk supports the json parameter. (#17)
        if isinstance(record, six.string_types):
            record = json.loads(record)
        record = record.copy()
        record_id = record.pop('id')
        headers = {}
        if 'headers' in kwargs:
            headers = kwargs.pop('headers')
        headers['Content-Type'] = 'application/json; charset=utf-8'

        return self._request('put', '/storage/%s/%s' % (
            collection.lower(), record_id), data=json.dumps(record),
            headers=headers, **kwargs)
