# auth.py
#
# Copyright 2023 Cédric Bellegarde
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio, GObject, GLib
from os import write, close

class Auth(GObject.Object):
    __gsignals__ = {
        "auth-result": (GObject.SignalFlags.RUN_FIRST, None, (bool,))
    }

    def __init__(self):
        GObject.Object.__init__(self)

    def do(self, username, password):
        """
            Try authenticating with user and password
        """
        if GLib.find_program_in_path("flatpak-spawn") is not None:
            argv = ["flatpak-spawn", "--host", "su", username, "-c", "/bin/true"]
        else:
            argv = ["su", username, "-c", "/bin/true"]

        try:
            (pid, stdin, stdout, stderr) = GLib.spawn_async(
                argv, flags=GLib.SpawnFlags.SEARCH_PATH |
                            GLib.SpawnFlags.STDOUT_TO_DEV_NULL |
                            GLib.SpawnFlags.DO_NOT_REAP_CHILD,
                standard_input=True,
                standard_output=False,
                standard_error=False
            )
            GLib.child_watch_add(
                GLib.PRIORITY_LOW, pid, self.on_process_reap
            )
            if stdin is not None:
                write(stdin, password.encode("utf-8"))
                close(stdin)
            
        except Exception as e:
            print(e)

    def on_process_reap(self, pid, ret):
        GLib.spawn_close_pid(pid)
        self.emit("auth-result", ret==0)
