# window.py
#
# Copyright 2023 Cédric Bellegarde
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio, GObject, GLib

from socket import socket, AF_INET, SOCK_STREAM
from threading import Thread

class Network(GObject.Object):
    __gsignals__ = {
        "server-available": (GObject.SignalFlags.RUN_FIRST, None, (bool,))
    }

    TIMEOUT = 2000

    def __init__(self, settings):
        GObject.Object.__init__(self)
        self.monitor = Gio.NetworkMonitor.get_default()
        self.settings = settings
        self.cancellable = None
        self.reachable = False
        self.timeout_id = None
       
        self.monitor.connect("network-changed", self.on_network_changed)

    def check_ldap_server(self):
        """
            Check if LDAP server is reachable
        """
        self.timeout_id = None
        self.cancel()
        self.cancellable = Gio.Cancellable.new()

        thread = Thread(target=self.check_port,
                        args=(self.settings.ldap_server, self.settings.ldap_port))
        thread.daemon = True
        thread.start()

    def cancel(self):
        if self.cancellable is not None:
            self.cancellable.cancel()
            self.cancellable = None

    def check_port(self, address, port):
        try:
            sock = socket(AF_INET, SOCK_STREAM)
            sock.settimeout(5)
            result = sock.connect_ex((address, port))
            self.reachable = result == 0
        except Exception as e:
            print(e)
            self.reachable = False
        self.emit("server-available", self.reachable)

    def on_network_changed(self, monitor, available):
        self.cancellable = Gio.Cancellable.new()
        if available:
            if self.timeout_id is not None:
                GLib.source_remove(self.timeout_id)
            self.timeout_id = GLib.timeout_add(
                Network.TIMEOUT, self.check_ldap_server
            )
        else:
            self.cancel()
            self.emit("server-available", False)
