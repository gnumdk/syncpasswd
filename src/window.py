# window.py
#
# Copyright 2023 Cédric Bellegarde
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

import os

from gi.repository import Adw
from gi.repository import GObject, Gtk, Gio, GLib

from .auth import Auth
from .firefox import Firefox
from .network import Network
from .secrets import Secrets
from .settings import Settings
from .wifi import Wifi


@Gtk.Template(resource_path='/org/adishatz/syncpasswd/window.ui')
class SyncPasswdWindow(Adw.ApplicationWindow):
    __gtype_name__ = 'SyncPasswdWindow'

    banner = Gtk.Template.Child()
    label = Gtk.Template.Child()
    sync_button = Gtk.Template.Child()
    spinner = Gtk.Template.Child()
    username = Gtk.Template.Child()
    previous_password = Gtk.Template.Child()
    new_password = Gtk.Template.Child()
    services = Gtk.Template.Child()
    scrolled = Gtk.Template.Child()
    toast_overlay = Gtk.Template.Child()

    SERVICES = {
        _("Your keyring") : "sync-secrets",
        _("WiFi connections") : "sync-wifi",
        _("Firefox") : "sync-firefox"
    }

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.running = 0
        
        self.secrets = Secrets()
        self.firefox = Firefox(self.secrets)
        self.settings = Settings()
        self.wifi = Wifi()

        if self.settings.ldap_server != "":
            self.auth = Auth()
            self.network = Network(self.settings)
            self.auth.connect("auth-result", self.on_auth_result)
            self.network.connect("server-available", self.on_server_available)
            self.scrolled.set_min_content_height(430)
        else:
            self.username.hide()
            self.auth = None
            self.network = None
            self.banner.hide()
            self.scrolled.set_min_content_height(400)

        self.previous_password.grab_focus()

        self.firefox.connect("connected", self.on_firefox_connected)
        self.firefox.connect("password-finished", self.on_finished)
        self.secrets.connect("master-result", self.on_master_result)
        self.secrets.connect("password-finished", self.on_finished)
        self.wifi.connect("password-finished", self.on_finished)
        
        self.username.set_text(GLib.get_user_name())

        for service in SyncPasswdWindow.SERVICES:
            row = Adw.SwitchRow.new()
            row.set_title(service)
            self.settings.bind(
                SyncPasswdWindow.SERVICES[service],
                row,
                "active",
                Gio.SettingsBindFlags.DEFAULT
            )
            self.services.add_row(row)

    def add_toast(self, message):
        toast = Adw.Toast.new(message)
        self.toast_overlay.add_toast(toast)

    def check_sync_button(self):
        """
            Mark sync button sensitive if all is OK
        """
        if self.network is not None and not self.network.reachable:
            self.sync_button.set_sensitive(False)
            return

        if not self.username.get_text():
            self.sync_button.set_sensitive(False)
            return

        if not self.new_password.get_text():
            self.sync_button.set_sensitive(False)
            return

        self.sync_button.set_sensitive(True)

    def check_finished(self):
        if self.running <= 0:
            self.check_sync_button()
            self.spinner.stop()

    def update_label(self, password_update):
        if password_update:
            items = []
            self.label.set_text(_("Enter your previous password below "\
                      "to update it in services")
            )
        else:
            self.label.set_text(
                _("""Enter user password to """ +
                  """activate account on this computer""")
            )

    @property
    def has_secrets(self):
        return self.secrets.service is not None and\
            self.settings.get_boolean("sync-secrets")

    @property
    def has_wifi(self):
        return self.wifi.client is not None and\
            self.settings.get_boolean("sync-wifi")

    @property
    def has_firefox(self):
        return self.firefox.connected and\
            self.settings.get_boolean("sync-firefox")

    @property
    def has_auth(self):
        return self.auth is not None

    def on_firefox_connected(self, firefox, status):
        self.update_label(self.username.get_text() == GLib.get_user_name())
        if status:
            self.add_toast(_("Firefox account connected"))
        elif self.settings.get_boolean("sync-firefox"):
            self.add_toast(_("Firefox account not connected"))

    def on_server_available(self, network, available):
        self.check_sync_button()
        if available:
            self.banner.set_title(
                "%s %s" % (
                    self.settings.domain,
                    # Translators: about network domain
                    _("available")
                )
            )
        else:
            self.banner.set_title(
                "%s %s" % (
                    _("You may need a VPN connection to"),
                    self.settings.domain
                )
            )

    def on_auth_result(self, auth, result):
        self.running -= 1
        if self.previous_password.get_sensitive() == False:
            if result:
                self.add_toast(
                    _("%s account activated") % self.username.get_text()
                )
            else:
                self.add_toast(_("%s: Invalid password"))
        else:
            if result:
                self.add_toast(_("Local password updated"))
            else:
                self.add_toast(_("Local password not updated"))
        self.check_finished()

    def on_master_result(self, secrets, result):
        self.running -= 1
        if result:
            self.add_toast(_("Keyring password updated"))
        else:
            self.add_toast(_("Keyring password not updated"))
            self.check_finished()
    
    def on_finished(self, service, message):
        self.running -= 1
        if message is not None:
            self.add_toast(message)
        self.check_finished()

    @Gtk.Template.Callback("username_changed")
    def on_username_changed(self, entry):
        username = entry.get_text()
        self.previous_password.set_sensitive(username == GLib.get_user_name())
        self.update_label(username == GLib.get_user_name())
        self.check_sync_button()

    @Gtk.Template.Callback("password_changed")
    def on_password_changed(self, entry):
        self.check_sync_button()

    @Gtk.Template.Callback("password_activate")
    def on_password_activate(self, entry):
        if self.sync_button.get_sensitive():
            self.on_sync_clicked(self.sync_button)

    @Gtk.Template.Callback("sync_clicked")
    def on_sync_clicked(self, button):
        button.set_sensitive(False)
        self.spinner.start()
        if self.auth is not None:
            self.running = 1
            self.auth.do(self.username.get_text(), self.new_password.get_text())
        if self.previous_password.get_text() != "":
            if self.has_secrets:
                self.running += 1
                self.secrets.reset_master_password(
                    self.previous_password.get_text(),
                    self.new_password.get_text()
                )
                self.running += 1
                self.secrets.reset_password(
                    self.previous_password.get_text(),
                    self.new_password.get_text()
                )
            if self.has_wifi:
                self.running += self.wifi.reset_secrets(
                    self.previous_password.get_text(),
                    self.new_password.get_text()
                )
            if self.has_firefox:
                self.running += 1
                self.firefox.reset_secrets(
                    self.previous_password.get_text(),
                    self.new_password.get_text()
                )
        else:
            self.check_finished()
