# wifi.py
#
# Copyright 2023 Cédric Bellegarde
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio, GObject, GLib, NM

class Wifi(GObject.Object):
    __gsignals__ = {
        "password-finished": (GObject.SignalFlags.RUN_FIRST, None, (str,))
    }

    def __init__(self):
        GObject.Object.__init__(self)
        try:
            self.client = NM.Client.new(None)
        except Exception as e:
            print(e)
            self.client = None

    def reset_secrets(self, old_password, new_password):
        if self.client is None:
            return 0
        GLib.timeout_add(250, self.check_connections, old_password, new_password)
        return len(self.client.get_connections())

    def check_connections(self, old_password, new_password):
        for connection in self.client.get_connections():
            self.check_secrets(connection, old_password, new_password)

    def check_secrets(self, connection, old_password, new_password):
        settings = connection.get_setting_802_1x()
        if settings is None:
            self.emit("password-finished", None)
            return

        connection.get_secrets_async(
            settings.get_name(),
            None,
            self.on_get_secrets,
            settings.get_name(),
            old_password,
            new_password)

    def update_secrets(self, connection, secrets, name, old_password, new_password):
        message = None
        count = 0
        try:
            hash = {}
            for root_key in secrets.keys():
                v = secrets.lookup_value(root_key)
                if v.get_size() == 0:
                    hash[root_key] = {}
                else:
                    hash[root_key] = v
                    if root_key == '802-1x':
                        hash_802_1x = {}
                        for key in hash[root_key].keys():
                            value = hash[root_key][key]
                            if key == 'password' and value == old_password:
                                count += 1
                                hash_802_1x['password'] = GLib.Variant(
                                    's', new_password)
                            else:
                                hash_802_1x[key] = GLib.Variant('s', value)
                        hash[root_key] = hash_802_1x

            new_secrets =  GLib.Variant('a{sa{sv}}', hash)

            connection.update_secrets(name, new_secrets)
            cloned = NM.SimpleConnection.new_from_dbus(
                connection.to_dbus(NM.ConnectionSerializationFlags.ALL)
            )
            connection.update2(
                cloned.to_dbus(NM.ConnectionSerializationFlags.ALL),
                NM.SettingsUpdate2Flags.TO_DISK,
                None,
                None
            )
        except Exception as e:
            print(e)
        if count == 1:
            message = _("One password WiFi updated")
        elif count > 1:
            message = _("%s passwords WiFi updated" % count)
        self.emit("password-finished", message)

    def on_get_secrets(self, connection, result, name, old_password, new_password):
        secrets = connection.get_secrets_finish(result)
        if secrets is not None:
            self.update_secrets(connection, secrets, name, old_password, new_password)
