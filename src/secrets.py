# secrets.py
#
# Copyright 2023 Cédric Bellegarde
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio, GObject, GLib, Secret

class Secrets(GObject.Object):
    __gsignals__ = {
        "master-result": (GObject.SignalFlags.RUN_FIRST, None, (bool,)),
        "password-finished": (GObject.SignalFlags.RUN_FIRST, None, (str,))
    }

    def __init__(self):
        GObject.Object.__init__(self)
        try:
            self.connection = Gio.bus_get_sync(
                Gio.BusType.SESSION, None
            )
            self.service = Secret.Service.get_sync(
                Secret.ServiceFlags.OPEN_SESSION, None
            )
            # Initial password lookup, prevent a lock issue in Flatpak backend
            if GLib.file_test("/app", GLib.FileTest.EXISTS):
                SecretSchema = {
                    "sync": Secret.SchemaAttributeType.STRING
                }
                SecretAttributes = {
                    "sync": "firefox"
                }
                schema = Secret.Schema.new("org.adishatz.syncpasswd",
                                           Secret.SchemaFlags.NONE,
                                           SecretSchema)
                Secret.password_lookup_sync(schema, SecretAttributes)
        except Exception as e:
            print(e)
            self.connection = None
            self.service = None

    def reset_master_password(self, old_password, new_password):
        """
            Reset Secrets master password using new password
        """
        old_value = Secret.Value(old_password, len(old_password), "text/plain")
        new_value = Secret.Value(new_password, len(new_password), "text/plain")
        
        self.connection.call(
            "org.gnome.keyring",
            "/org/freedesktop/secrets",
            "org.gnome.keyring.InternalUnsupportedGuiltRiddenInterface",
            "ChangeWithMasterPassword",
            GLib.Variant("(o(oayays)(oayays))",
                         ("/org/freedesktop/secrets/collection/login",
                         self.service.encode_dbus_secret(old_value),
                         self.service.encode_dbus_secret(new_value))),
            None,
            Gio.DBusCallFlags.NONE,
            -1,
            None,
            self.on_master_password_resetted
        )

    def reset_password(self, old_password, new_password):
        Secret.Collection.for_alias(
            self.service,
            Secret.COLLECTION_DEFAULT,
            Secret.CollectionFlags.LOAD_ITEMS,
            None,
            self.on_alias,
            old_password,
            new_password
        )

    def get_firefox_sync(self, callback, *args):
        """
            Get Firefox Sync password
        """
        try:
            SecretSchema = {
                "sync": Secret.SchemaAttributeType.STRING
            }
            SecretAttributes = {
                "sync": "firefox"
            }
            schema = Secret.Schema.new(
                "org.adishatz.syncpasswd",
                Secret.SchemaFlags.NONE,
                SecretSchema
            )
            Secret.password_search(
                schema, SecretAttributes,
                Secret.SearchFlags.UNLOCK |
                Secret.SearchFlags.LOAD_SECRETS,
                None,
                self.on_secret_search,
                callback,
                *args
            )
        except Exception as e:
            print(e)

    def set_firefox_sync(self, login, password, callback=None, *args):
        """
            Set Firefox Sync password
        """
        try:
            schema_string = "org.adishatz.syncpasswd"
            SecretSchema = {
                "sync": Secret.SchemaAttributeType.STRING,
                "login": Secret.SchemaAttributeType.STRING,
            }
            schema = Secret.Schema.new(
                "org.adishatz.syncpasswd",
                Secret.SchemaFlags.NONE,
                SecretSchema
            )
            SecretAttributes = {
                "sync": "firefox",
                "login": login,
            }
            Secret.password_store(
                schema, SecretAttributes,
                Secret.COLLECTION_DEFAULT,
                schema_string,
                password,
                callback,
                *args
            )
        except Exception as e:
            print(e)

    def clear_firefox_sync(self, callback=None, *args):
        """
            Clear Firefox Sync password
        """
        try:
            SecretSchema = {
                "sync": Secret.SchemaAttributeType.STRING
            }
            SecretAttributes = {
                "sync": "firefox"
            }
            schema = Secret.Schema.new(
                "org.adishatz.syncpasswd",
                Secret.SchemaFlags.NONE,
                SecretSchema
            )
            Secret.password_clear(
                schema,
                SecretAttributes,
                None,
                self.on_clear_search,
                callback,
                *args
            )
        except Exception as e:
            print(e)


    def on_alias(self, source, result, old_password, new_password):
        message = None
        count = 0
        try:
            collection = Secret.Collection.for_alias_finish(result)
            for item in collection.get_items():
                item.load_secret_sync(None)
                secret = item.get_secret()
                try:
                    if secret is not None and secret.get_text() == old_password:
                        value = Secret.Value(
                            new_password, len(new_password), "text/plain"
                        )
                        item.set_secret_sync(value)
                        count += 1
                except:
                    pass
        except Exception as e:
            print(e)
        if count == 1:
            message = _("One password updated in keyring")
        elif count > 1:
            message = _("%s passwords updated in keyring" % count)
        self.emit("password-finished", message)

    def on_master_password_resetted(self, connection, task):
        try:
            result = connection.call_finish(task)
            self.emit("master-result", True)
        except Exception as e:
            self.emit("master-result", False)

    def on_secret_search(self, source, result, callback, *args):
        try:
            items = Secret.password_search_finish(result)
            if not items:
                raise Exception("No firefox sync account")

            count = len(items)
            index = 0
            for item in items:
                attributes = item.get_attributes()
                secret = item.retrieve_secret_sync()
                if callback is not None:
                    callback(
                        attributes,
                        secret.get().decode('utf-8'),
                        index,
                        count,
                        *args
                    )
                index += 1
        except Exception as e:
            print(e)
            if callback is not None:
                callback(None, None, 0, 0, *args)

    def on_clear_search(self, source, result, callback, *args):
        try:
            Secret.password_clear_finish(result)
            if callback is not None:
                callback(*args)
        except Exception as e:
            print(e)
            if callback is not None:
                callback(None, None, 0, 0, *args)
