# firefox_dialog.py
#
# Copyright 2023 Cédric Bellegarde
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Adw
from gi.repository import GObject, Gtk, Gio, GLib

from .firefox import Firefox


@Gtk.Template(resource_path='/org/adishatz/syncpasswd/firefox_dialog.ui')
class FirefoxDialog(Adw.Window):
    __gtype_name__ = 'FirefoxDialog'

    content = Gtk.Template.Child()
    label = Gtk.Template.Child()
    username = Gtk.Template.Child()
    password = Gtk.Template.Child()
    twofa = Gtk.Template.Child()
    spinner = Gtk.Template.Child()
    connect_button = Gtk.Template.Child()
    toast_overlay = Gtk.Template.Child()

    def __init__(self, main_window):
        super().__init__(transient_for=main_window)
        self.main_window = main_window
        self.firefox = main_window.firefox
        self.secrets = main_window.secrets

        self.firefox.connect("verify-account", self.on_verify_account)
        self.firefox.connect("connected", self.on_connected)

        self.username.grab_focus()

        self.secrets.get_firefox_sync(self.on_get_firefox_sync)

    def on_verify_account(self, firefox):
        toast = Adw.Toast.new(_("Check your emails, then finish account validation"))
        self.toast_overlay.add_toast(toast)
        self.connect_button.set_label(_("Finish account validation"))
        self.connect_button.set_sensitive(True)

    def on_connected(self, firefox, status):
        self.spinner.stop()
        if status:
            self.close()
        else:
            self.content.set_sensitive(True)
            toast = Adw.Toast.new(_("Connection failed"))
            self.toast_overlay.add_toast(toast)
            self.connect_button.set_label(_("Connect"))

    def on_get_firefox_sync(self, attributes, password, index, count):
        try:
            if attributes is None:
                return

            self.label.set_text(_("Connected to Firefox Sync"))
            self.content.set_sensitive(False)
            self.connect_button.set_sensitive(True)
            self.connect_button.set_label(_("Disconnect"))
            self.connect_button.get_style_context().remove_class(
                "suggested-action"
            )
            self.connect_button.get_style_context().add_class(
                "destructive-action"
            )
            self.username.set_text(attributes["login"])
        except Exception as e:
            print(e)

    @Gtk.Template.Callback("connect_clicked")
    def on_connect_clicked(self, button):
        # Connect
        if self.firefox.status != Firefox.Status.CONNECTED:
            self.content.set_sensitive(False)
            self.connect_button.set_sensitive(False)
            self.firefox.logon(
                self.username.get_text(),
                self.password.get_text(),
                self.twofa.get_text()
            )
            self.spinner.start()
        # Disconnect
        else:
            self.firefox.logout()
            self.close()

    @Gtk.Template.Callback("cancel_clicked")
    def on_cancel_clicked(self, button):
        self.spinner.stop()
        self.close()

    @Gtk.Template.Callback("entry_changed")
    def on_entry_changed(self, button):
        if self.content.get_sensitive():
            self.connect_button.set_sensitive(
                self.username.get_text() != "" and
                self.password.get_text() != ""
            )

    @Gtk.Template.Callback("password_activate")
    def on_password_activate(self, entry):
        if self.connect_button.get_sensitive():
            self.on_connect_clicked(self.connect_button)
            
