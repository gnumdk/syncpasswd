��            )         �     �  1   �     �     �       
        )  8   5  B   n     �     �     �               '     8     S     j     �     �     �     �     �  $   �     �     �      �          4  	   B  �   L     D  >   L  	   �     �     �     �     �  R   �  i   8      �     �  )   �  %        ,     9  "   K     n     �     �     �     �     �     �  .   
	  !   9	     [	  )   g	     �	     �	  
   �	     
                              	                                                                                                     Cancel Check your emails, then finish account validation Connect Connected to Firefox Sync Connection failed Disconnect Domain name Enter user password to activate account on this computer Enter your previous password below, it will be updated in: keyring Finish account validation Firefox account connected Keyring password not updated Keyring password updated LDAP server LDAP server port Local password not updated Local password updated Looking for domain… New password Password Password sync Previous password Sync Two factor authentication (optional) Update your password Username You may need a VPN connection to _About Sync Password _Firefox Sync available Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-09-28 10:21+0200
Last-Translator: 
Language-Team: 
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 3.3.1
 Annuler Vérifiez vos emails, ensuite terminez la validation du compte Connecter Connecté à Firefox Sync Échec de la connexion Se déconnecter Nom de domaine Entrez le mot de passe de l'utilisateur pour activer son compte sur cet ordinateur Entrez votre mot de passe précédent ci dessous, il sera mis à jour dans: le trousseau de mots de passe Terminer la validation du compte Compte Firefox connecté Mot de passe du trousseau non mis à jour Mot de passe du trousseau mis à jour Serveur LDAP Port serveur LDAP Mot de passe local non mis à jour Mot de passe local mis à jour Recherche du domaine… Nouveau mot de passe Mot de passe Synchronisation mot de passe Mot de passe précédent Synchroniser Authentification à deux facteurs (facultatif) Mettre à jour votre mot de passe Utilisateur Vous avez peut être besoin d'un VPN pour _À propos de Sync Password _Firefox Sync disponible 