#!/bin/bash

function generate_po()
{
    ls src/*.py src/*.ui data/org.adishatz.syncpasswd.gschema.xml data/org.adishatz.syncpasswd.desktop.in > po/POTFILES
    xgettext --from-code=UTF-8 $(cat po/POTFILES) -o po/syncpasswd.pot
    sed -i 's/CHARSET/UTF-8/g' po/syncpasswd.pot
    while read lang
    do
        [ ! -e po/$lang.po ] && touch po/$lang.po
        msgmerge -N po/$lang.po po/syncpasswd.pot > /tmp/$$$lang.po
        mv /tmp/$$$lang.po po/$lang.po
    done < po/LINGUAS
}

generate_po
